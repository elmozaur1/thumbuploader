# thumbUploader

Zadanie rekrutacyjne:
zaimplementować w technologii PHP mechanizm tworzenia miniaturek obrazków z lokalnego dysku i zapisywania ich we wskazanym miejscu. 

## Wymagania
- rozmiar dłuższego boku obrazka po przeskalowaniu to maksymalnie 150px; 
- miejscem docelowym zapisywanych plików powinno być jedno z wybranych rozwiązań chmurowych (folder na serwerze FTP, folder w usłudze Dropbox, bucket S3)
- mechanizm ma mieć formę komendy uruchamianej z wiersza poleceń
- rozwiązanie należy oprzeć o dowolnie wybrany framework

## Pobranie i Uruchomienie
pobrać projekt do folderu poleceniem: 
> git clone https://gitlab.com/elmozaur1/thumbuploader.git

przejść do folderu projektu:
> cd thumbuploader

zainstalować zależności:
> composer install

konfiguracja dostępu do FTP oraz S3 znajduje się w pliku services.yml oraz .env

Uruchomienie:
> php bin/console app:thumb-upload

po uruchomieniu, skrypt poprosi o podanie ścieżki do folderu ze zdjęciami
> d:/pic
(skrypt posiada walidację na pusty katalog oraz string który nie jest katalogiem - 2 próby)

w kolejnym kroku należy wybrać miejsce docelowe (defaultowo dostępne są 3 exportery)
[0] local
[1] ftp
[2] s3
> 1

o poprawnie przetworzonym pliku zostaniemy poinformowani:
tmp/5e9d25-62ebecb4bd103.jpg     [OK]



