<?php

namespace App\Command;

use App\Service\ExternalExporter;
use App\Service\ImageProcess;
use Exception;
use FilesystemIterator;
use RuntimeException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Thumbnail Upload Command used to get local image, scale it and send to: server folder,
 * ftp server or s3 bucket. 
 * 
 * This command uses two services:
 * ExternalExporter - to connect and send image do the destination folder (each exporter
 * is placed in Service/Exporters directory)
 * ImageProcess - to check if file is image, scale it and save in tmp folder.
 * 
 * usage : php bin/console app:thumb-upload
 * After init user is asked to enter local directory with images (png/jpg/gif)
 * example: d:/pic
 * next step is to chose destination path (in this example: 
 * server local dir "public/pic",
 * free ftp server (files are deleted after 30min), 
 * or S3 bucket).
 * 
 * @author Grzegorz Waletko <waletkog@gmail.com>
 */
#[AsCommand(name: 'app:thumb-upload')]
class ThumbUploadCommand extends Command
{
    public function __construct(public ImageProcess $imageProcess, public ExternalExporter $externalExporter, private ParameterBagInterface $paramBag)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp('This command allows you to create thumbnail and upload it to the cloud')
            ->setDescription('This command allows you to create thumbnail and upload it to the cloud')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');

        $sourcePathQuestion = new Question('Please enter directory name : ', 'pic');
        $sourcePathQuestion->setValidator(function ($answer) {
            if (!is_dir($answer)) {
                throw new RuntimeException(
                    'This is not a directory'
                );
            } else if ($this->isDirEmpty($answer)) {
                throw new RuntimeException(
                    'Directory is empty'
                );
            }

            return $answer;
        });
        $sourcePathQuestion->setMaxAttempts(2);
        $sourcePath = $helper->ask($input, $output, $sourcePathQuestion);

        $destinationPathQuestion = new ChoiceQuestion(
            'Please select your destination path (default is local)',
            $this->getExportersArray(),
            0
        );
        $destinationPathQuestion->setErrorMessage('Destination path %s is invalid.');
        $destinationPath = $helper->ask($input, $output, $destinationPathQuestion);

        if ($handle = opendir($sourcePath)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry !== '.' && $entry !== '..')
                {
                    $this->imageProcess->loadFile($sourcePath.'/'.$entry);

                    if ($this->imageProcess->checkIfIsImage())
                    {
                        try {
                            $this->imageProcess->loadImageData();
                            $this->imageProcess->scaleImage();
                            $image = $this->imageProcess->saveImage();

                            $this->externalExporter->sendImage($image, $destinationPath, $output);
                            $this->externalExporter->clearTmpFile($image);

                        } catch (Exception $e) {
                            $output->writeln('<comment>Unknown image file : ' . $e. '</comment>');
                        }

                    }
                }
            }
        
            closedir($handle);
            $this->externalExporter->closeConnection();
        }
        
        return Command::SUCCESS;
    }

    /**
     * getExportersArray - based on config in services.yml method creates available
     * exporters array
     * 
     * @return array
     */
    public function getExportersArray(): array
    {
        $fileExporters = $this->paramBag->get('thumbExporters');
        return array_keys($fileExporters);
    }

    /**
     * isDirEmpty - check if dir is empty
     *
     * @param $dir
     * @return bool|null
     */
    public function isDirEmpty($dir): ?bool
    {
        if (!is_readable($dir)) return null;

        return !(new FilesystemIterator($dir))->valid();
    }
}