<?php

namespace App\Service\Exporters;

interface FileExporterInterface
{
    public function sendFile(string $file);
    public function closeConnection();
}