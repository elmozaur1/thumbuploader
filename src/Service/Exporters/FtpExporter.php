<?php

namespace App\Service\Exporters;

use FTP\Connection as FTPConnection;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * FTP Exporter for - ExternalExporter service.
 * this exporter can push file to FTP server.
 * 
 * @author Grzegorz Waletko <waletkog@gmail.com>
 */
class FtpExporter implements FileExporterInterface
{
    /** @var array - configuration array */
    private array $paramBag;

    /** @var FTPConnection - ftp connection handler */
    private FTPConnection $connectionHandler;
    
    public function __construct(ParameterBagInterface $paramBag)
    {
        $this->paramBag = $paramBag->get('thumbExporters')['ftp'];

        $this->createConnection();
    }

    /**
     * createConnection - create connection to ftp server.
     */
    public function createConnection(): void
    {
        $this->connectionHandler = ftp_connect($this->paramBag['ftpServer']);
        ftp_login($this->connectionHandler, $this->paramBag['ftpUserName'], $this->paramBag['ftpUserPassword']);
    }

    /**
     * sendFile - sends file to ftp server.
     *
     * @param string $file
     * @return bool
     */
    public function sendFile(string $file): bool
    {
        $remote_file = basename($file);

        return ftp_put($this->connectionHandler, $remote_file, $file, FTP_BINARY);
    }

    /**
     * closeConnection - this method is for close FTP connection
     */
    public function closeConnection(): void
    {
        ftp_close($this->connectionHandler);
    }
}