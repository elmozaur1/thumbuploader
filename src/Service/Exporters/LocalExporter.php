<?php

namespace App\Service\Exporters;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Local Exporter for - ExternalExporter service.
 * this exporter can save file in public path.
 * 
 * @author Grzegorz Waletko <waletkog@gmail.com>
 */
class LocalExporter implements FileExporterInterface
{
    /** @var ParameterBagInterface - configuration array*/
    private ParameterBagInterface $paramBag;

    /** @var string $destinationPath - public path to save file */
    private string $destinationPath = 'public/pic';

    public function __construct(ParameterBagInterface $paramBag)
    {
        $this->paramBag = $paramBag;

        if (!is_dir($this->destinationPath)) {
            mkdir($this->destinationPath, 0700);
        }
    }

    /**
     * sendFile - this method saves source file in destination path
     * 
     * @param string $file
     * @return bool
     */
    public function sendFile(string $file): bool
    {
        $destinationPath = $this->destinationPath . '/' . basename($file);

        return copy($file, $destinationPath);
    }

    /**
     * closeConnection - this method is for close connection - if necessary
     */
    public function closeConnection(): void
    {
        // no need to close
    }
}