<?php

namespace App\Service\Exporters;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * S3 Exporter for - ExternalExporter service.
 * this exporter can push file to S3 storage.
 * 
 * @author Grzegorz Waletko <waletkog@gmail.com>
 */
class S3Exporter implements FileExporterInterface
{
    /** @var array - configuration array */
    private array $paramBag;

    /** @var S3Client - S3 connection handler */
    private S3Client $connectionHandler;

    public function __construct(ParameterBagInterface $paramBag)
    {
        $this->paramBag = $paramBag->get('thumbExporters')['s3'];

        $this->createConnection();
    }
    
    /**
     * createConnection - create connection to S3 storage.
     */
    public function createConnection()
    {
        $this->connectionHandler = new S3Client([
            'region' => 'eu-west-2',
            'version' => 'latest',
            'credentials' => [
                'key' => $this->paramBag['key'],
                'secret' => $this->paramBag['secret']
            ]
        ]);
    }

    /**
     * sendFile - sends file to s3 storage.
     *
     * @param string $file
     * @return bool
     */
    public function sendFile(string $file): bool
    {
        $remote_file = basename($file);
        $result = false;
        try {
            $resultObj = $this->connectionHandler->putObject([
                'Bucket' => $this->paramBag['bucket'],
                'Key' => $remote_file,
                'Body' => fopen($file, 'rb'),
                'ACL' => 'public-read'
            ]);
            $result = $resultObj['@metadata']['statusCode'] === 200;

        } catch (S3Exception $e) {
            print($e->getMessage());
        }



        return $result;
    }

    /**
     * closeConnection - this method is for close connection
     */
    public function closeConnection(): void
    {
        // no need to close
    }
}