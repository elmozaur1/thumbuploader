<?php

namespace App\Service;

use Exception;
use GdImage;

/**
 * ImageProcess - class provides image service - load, check, scale, save
 * 
 * @author Grzegorz Waletko <waletkog@gmail.com>
 */
class ImageProcess
{
    /** @var string $imgName - file name */
    private string $imgName;

    /** @var string $imgExtension - file extension */
    private string $imgExtension;

    /** @var string $sourceDir - file source directory */
    private string $sourceDir;

    /** @var GdImage $imgTmpData - temp image file */
    private GdImage $imgTmpData;

    /** @var int $maxImgWidth - max thumbnail width */
    private int $maxImgWidth = 150;

    /** @var int $maxImgHeight - max thumbnail height */
    private int $maxImgHeight = 150;

    /** @var int $maxImageQuality - compression quality 100 = best */
    private int $maxImageQuality = 100;

    /** @var int $currentImgWidth - source image width */
    private int $currentImgWidth;

    /** @var int $currentImgHeight - source image height */
    private int $currentImgHeight;

    /** @var string $currentImgMime - source image mime type */
    private string $currentImgMime;

    /** @var string $tmpDir - directory for tmp file */
    private string $tmpDir = 'tmp';

    /**
     * loadFile - load file (maybe image) and get info about: name, extension and source dir
     * 
     * @param string $img
     */
    public function loadFile(string $img): void
    {
        $pathParts = pathinfo($img);

        $this->imgName = $pathParts['filename'];
        $this->imgExtension = $pathParts['extension'];
        $this->sourceDir = $pathParts['dirname'];
    }

    /**
     * checkIfIsImage - check if file is image (has width and height)
     * 
     * @return bool
     */
    public function checkIfIsImage(): bool
    {
        $imageInfo = getimagesize($this->sourceDir.'/'.$this->imgName.'.'.$this->imgExtension);
        
        return is_array($imageInfo);
    }

    /**
     * loadImageData - based on source image format, creates destination picture format
     */
    public function loadImageData(): void
    {
        $filename = $this->sourceDir.'/'.$this->imgName.'.'.$this->imgExtension;
        $imageInfo = getimagesize($filename);
        $this->currentImgWidth = $imageInfo[0];
        $this->currentImgHeight = $imageInfo[1];
        $this->currentImgMime = $imageInfo['mime'];

        $this->imgTmpData = match(strtolower($this->imgExtension)) {
            'jpg', 'jpeg' => imagecreatefromjpeg($filename),
            'gif' => imagecreatefromgif($filename),
            'png' => imagecreatefrompng($filename),
            default => throw new Exception("Unknown image format", 1),
        };
    }

    /**
     * scaleImage - proportional scale image
     */
    public function scaleImage():void
    {
        if (($this->currentImgWidth > $this->maxImgWidth) || ($this->currentImgHeight > $this->maxImgHeight))
        {
            
            $ratio = $this->maxImgWidth / $this->currentImgWidth;
            $newWidth = $this->maxImgWidth;
            $newHeight = $this->currentImgHeight * $ratio;

            if ($newHeight > $this->maxImgHeight) {
                $ratio = $this->maxImgHeight / $this->currentImgHeight;
                $newHeight = $this->maxImgHeight;
                $newWidth = $this->currentImgWidth * $ratio;
            }

            $newImage = imagecreatetruecolor($newWidth, $newHeight);
            imagecopyresampled($newImage, $this->imgTmpData, 0, 0, 0, 0, $newWidth, $newHeight, $this->currentImgWidth, $this->currentImgHeight);
            $this->imgTmpData = $newImage;
            imagedestroy($newImage);
        }
    }

    /**
     * saveImage - save image file based on source extension
     * if necessary creates tmp directory
     * 
     * @return string (path where file was saved)
     */
    public function saveImage(): string
    {
        if (!is_dir($this->tmpDir))
        {
            mkdir($this->tmpDir, 0700);
        }

        $filename = substr(md5(time()), 0, 6).'-'.uniqid().'.'.strtolower($this->imgExtension);
        $savePath = $this->tmpDir.'/'.$filename;

        switch(strtolower($this->imgExtension))
	    {
	        case 'jpg':
	        case 'jpeg':
	            imagejpeg($this->imgTmpData, $savePath, $this->maxImageQuality);
	            break;

	        case 'gif':
	            imagegif($this->imgTmpData, $savePath);
	            break;

	        case 'png':
	            $invertScaleQuality = 9 - round(($this->maxImageQuality/100) * 9);
	            imagepng($this->imgTmpData, $savePath, $invertScaleQuality);
	            break;
	    }

        return $savePath;
    }

}