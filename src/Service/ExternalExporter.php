<?php

namespace App\Service;

use App\Service\Exporters\FileExporterInterface;
use Exception;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * ExternalExporter - class provides export service - create exporter, connect 
 * to destination, send image.
 * This service uses exporters from exporters dir. 
 * Exporters configuration stored in .env/service.yml files
 * 
 * @author Grzegorz Waletko <waletkog@gmail.com>
 */
class ExternalExporter
{
    /** @var ParameterBagInterface - configuration array*/
    private ParameterBagInterface $paramBag;

    /** @var FileExporterInterface - service with exporter to send file to ftp, cloud etc.*/
    private FileExporterInterface $exporter;

    public function __construct(ParameterBagInterface $paramBag)
    {
        $this->paramBag = $paramBag;
    }

    /**
     * sendImage - based on selected destination path and file - creates proper exporter obj
     * and executes sendFile method on it. Return state form sendFile is passed to showMessage
     * output
     *
     * @param string $image
     * @param string $destinationPath
     * @param OutputInterface $output
     * @throws Exception
     */
    public function sendImage(string $image, string $destinationPath, OutputInterface $output): void
    {
        try {
            $exporter = $this->getExporterByDestinationPath($destinationPath);
            $result = $this->sendFileToExternalDestination($exporter, $image);
            $this->showMessage($image, $result, $output);

        } catch (Exception $e) {
            $output->write('<comment>Exporter error : ' . $e . '</comment>');
        }

    }

    /**
     * getExporterByDestinationPath - based on selected destination path, this method gets
     * proper exporter class, creates exporter obj and returns it
     *
     * @param string $destinationPath
     * @return FileExporterInterface
     * @throws Exception
     */
    public function getExporterByDestinationPath(string $destinationPath): FileExporterInterface
    {
        $exporterClass = $this->getExporterClassByDestinationPath($destinationPath);

        if ($exporterClass === '') {
            throw new Exception('No config for selected exporter');
        }

        return $this->createExporterByClass($exporterClass);
    }

    /**
     * getExporterClassByDestinationPath - based on selected destination path, this method 
     * returns proper exporter class
     * 
     * @param string $destinationPath
     * @return string
     */
    public function getExporterClassByDestinationPath(string $destinationPath): string
    {
        $fileExportersArray = $this->paramBag->get('thumbExporters');
        $exporterClass = '';

        foreach ($fileExportersArray as $fileExporterName => $fileExporterClass) {
            if ($fileExporterName === $destinationPath) {
                $exporterClass = $fileExporterClass['class'];
            }
        }

        return $exporterClass;
    }

    /**
     * createExporterByClass - based on exporter class name creates exporter obj.
     * ParameterBag is injected to constructor
     *
     * @param string $exporterClass
     * @return FileExporterInterface
     */
    public function createExporterByClass(string $exporterClass): FileExporterInterface
    {
        $this->exporter = new $exporterClass($this->paramBag);
        return $this->exporter;
    }

    /**
     * sendFileToExternalDestination - executes sending process on proper exporter.
     * return bool state is passed to showMessage as $result var.
     * 
     * @param FileExporterInterface $exporter
     * @param string $image
     * @return bool
     */
    public function sendFileToExternalDestination(FileExporterInterface $exporter, string $image): bool
    {
        return $exporter->sendFile($image);
    }

    /**
     * showMessage - method shows output message on cmd/terminal.
     * return bool state is passed to showMessage as $result var.
     * 
     * @param string $file
     * @param bool $result
     * @param OutputInterface $output
     */
    public function showMessage(string $file, bool $result, OutputInterface $output): void
    {
        $outMsg = ($result) ? '<info>[OK]</info>' : '<error>[FAIL]</error>';
        $output->writeln($file . '     '.$outMsg);
    }

    /**
     * clearTmpFile - remove tmp file after processing.
     * 
     * @param string $image
     */
    public function clearTmpFile(string $image): void
    {
        unlink($image);
    }

    /**
     * closeConnection - if necessary closing connection on exporter (ex.for FTP).
     */
    public function closeConnection(): void
    {
        $this->exporter->closeConnection();
    }
}